dz2_agent
---------
The ROS agent package for Danger 'Zona 2.

# Agent State Machine

The agent has a simple state machine that loads a mission, runs it's tasks, and waits for a new mission to be run. A diagram of the state machine has been included below.

![State machine diagram of dz2 agent](docs/images/agent-state-machine.png)


# Mission

A mission is a collection of tasks that will be executed by a robot. The agent supports the ability to load different missions at runtime. This is done by changing the `enabled` parameter to point to the specific mission.


An example mission can be created from tasks as shown below.

```
missions:
    enabled: example_mission
    example_mission:
        - module: dz2_agent.tasks.delay
          class: Delay
          params:
            delay: 2.0

        - module: dz2_agent.tasks.noop
          class: NoopTask
          params: {}

       - module: dz2_agent.tasks.torp
         class: TestTorpedo
         params:
            torpedo: 1
```

The new configuration can be loaded manually running `rosparam load config/mission.yaml /dz2_agent`.

# Tasks

The main way to add new functionality to agent is through a `Task`. A new `Task` must implement at the minimum: `setup`, `execute`, `is_finished` and `cleanup`.

`setup` - is where an task initialization should be performed. Listening to a new topic.

`execute` - is where the task is performed. This method will be continue to be called until the `is_finished` condition is met.

`is_finished` - should report when the task has finished. This should only return `True` or `False`.

`cleanup` - Any task cleanup should be done. This could be unregistering from a topic.

`reset` - Used to reset a task back to an initial state.

A task try to parameterize as much configuration as possible in its constructor so that the task can be reused in a mission.

## Creating a Task

```
import task

class NoopTask(task.Task):
    def __init__(self, robot):
        super(self.__class__, self).__init__(robot, name=0.1)

    def setup(self):
        """
        No setup required
        """

    def execute(self):
        """
        Does nothing
        """

    def cleanup(self):
        """
        No Cleanup required
        """

    def reset(self):
        """
        Nothing to see here
        """

    def is_finished(self):
        return True
```

# Filters

The agent can process image data using filters. A `Filter` can then be used from a `Task`. Filters are implemented using
the `filters.Filter` class. This class has one method called `apply` that takes an OpenCV
image and returns a dictionary with relevant values for the given filter.

Filters are loaded from a `filters.yaml` configuration file where they can be parameterized for specific tasks. This requires that each filter have its own unique name.

All filters require a filter name and all configuration should be should be parameterized in the constructor so that it can be parameterized in the filter configuration file.

## Creating a filter

This demonstrates a simple pass through filter that also passes a message out.

```python
from base_filter import Filter

class PassThroughFilter(Filter):
    def __init__(self, name, message="My awesome filter"):
        super(PassThroughFilter, self).__init__(name)
        self.message = message

    def apply(self, image):
        return dict(image=image, message=self.message)
```

## Loading the filter
A filter can be added to the filters configuration file and loaded using `roslaunch dz2_agent agent.launch`. The filter can be parameterized to pass through various messages as shown below.

```
filters:
- module: dz2_agent.filters.passthrough
  class: PassThroughFilter
  name: awesome
  params:
    message: This is an awesome filter

- module: dz2_agent.filters.passthrough
  class: PassThroughFilter
  name: lame
  params:
    message: This is a lame filter
```

## Using your filter

After a filter is loaded it can be used be applied to any specific camera. The `process` function on the vision module expects the first argument to be the camera and second to be the name of the filter to be used.

```python
robot.vision.process('down', 'awesome')
{ 'image': '...', 'message': 'This is an awesome filter'}

robot.vision.process('down', 'lame')
{ 'image': '...', 'message': 'This is a lame filter'}
```

