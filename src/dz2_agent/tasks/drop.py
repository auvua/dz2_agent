import time
import task

class TestMarker(task.Task):
    def __init__(self, robot, marker, dt=0.1):
        super(self.__class__, self).__init__(robot, dt)

        self.marker = marker
        self.timeout = 0.5

    def setup(self):
        print "Firing marker {0}".format(self.marker)
        self.start = time.time()
        self.robot.pneumatics.fire_marker(self.marker)

    def execute(self):
        pass

    def cleanup(self):
        print "Done!"

    def reset(self):
        pass

    def is_finished(self):
        return (time.time() > self.start + self.timeout)
