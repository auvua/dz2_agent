import time
import task

class TestClaw(task.Task):
    def __init__(self, robot, close, dt=0.1):
        super(self.__class__, self).__init__(robot, dt)

        self.close = close
        self.timeout = 0.5

    def setup(self):
        self.start = time.time()
        if self.close:
            self.robot.pneumatics.close_claw()
            print("Closing claw")
        else:
            self.robot.pneumatics.open_claw()
            print("Opening claw")

    def execute(self):
        pass

    def cleanup(self):
        print "Done!"

    def reset(self):
        pass

    def is_finished(self):
        return (time.time() > self.start + self.timeout)
