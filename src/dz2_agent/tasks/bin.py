import time
import task
from utils.pid import PID
from geometry_msgs.msg import Wrench

class MoveToBin(task.Task):
    def __init__(self, robot, camera, filter_name, surge, sway, heave, ramp, dt=0.1):
        super(self.__class__, self).__init__(robot, dt)
        self.filter_name = filter_name
        self.camera = camera
        self.force = Wrench()
        self.force.force.x = surge
        self.force.force.y = sway
        self.force.force.z = heave
        self.ramp = ramp
        self.filtered = {}

    def setup(self):
        self.robot.motion.set_all_control(True)
        self.start_time = time.time()

    def execute(self):
        self.filtered = self.robot.vision.process(self.camera, self.filter_name)

        ramp_force = Wrench()

        if self.ramp > 0:
            scaling = min((time.time() - self.start_time) / self.ramp, 1.0)
        else:
            scaling = 1.0

        ramp_force.force.x = self.force.force.x * scaling
        ramp_force.force.y = self.force.force.y * scaling
        ramp_force.force.z = self.force.force.z * scaling

        self.robot.motion.move_surge(ramp_force.force.x)
        self.robot.motion.move_sway(ramp_force.force.y)
        self.robot.motion.move_heave(ramp_force.force.z)

    def cleanup(self):
        self.robot.motion.clear_force()

    def reset(self):
        pass

    def is_finished(self):
        return 'bin' in self.filtered and self.filtered['bin']


class AlignToBin(task.Task):
    def __init__(self, robot, camera, filter_name, depth, dt=0.1):
        super(self.__class__, self).__init__(robot, dt)
        self.filter_name = filter_name
        self.camera = camera
        self.target_depth = depth
        self.last_msg = time.time()
        self.last_seen = time.time()
        self.pid_surge = PID(0.25, 0.001, 0.0, -5.0, 5.0)
        self.pid_sway = PID(1.0, 0.001, 0.0, -5.0, 5.0)
        self.pid_yaw = PID(0.125, 0.0015, 0.0, -5.0, 5.0)

    def setup(self):
        self.start = time.time()
        self.robot.motion.set_all_control(True)
        self.robot.motion.set_yaw_control(False)
        self.robot.motion.set_depth(self.target_depth)
        self.pid_surge.enable = True
        self.pid_sway.enable = True
        self.pid_yaw.enable = True

    def execute(self):
        curr_time = time.time()
        filtered = self.robot.vision.process(self.camera, self.filter_name)
        self.robot.vision.publish('binfilter', filtered['image'])

        if 'bin' in filtered and filtered['bin']:
            keys = ['cx', 'cy', 'theta', 'area']
            cx, cy, theta, area = [filtered[k] for k in keys]
            print("Bin found")
            self.last_seen = time.time()
            self.pid_surge.enable = True
            self.pid_sway.enable = True
            self.pid_yaw.enable = True
            self.robot.motion.move_surge(self.pid_surge.update(curr_time, cy))
            self.robot.motion.move_sway(self.pid_sway.update(curr_time, cx))
            self.robot.motion.move_yaw(self.pid_yaw.update(curr_time, theta))
        else:
            if curr_time - self.last_msg > 1.0:
                self.last_msg = curr_time
                print("Searching for bin")
                self.robot.leds.set_leds_strobe(255, 0, 0, time=0.2)
            if curr_time - self.last_seen > 0.5:
                self.pid_surge.enable = False
                self.pid_sway.enable = False
                self.pid_yaw.enable = False
                self.robot.motion.move_surge(0.0)
                self.robot.motion.move_sway(0.0)
                self.robot.motion.move_yaw(0.0)

    def cleanup(self):
        self.robot.motion.clear_force()
        self.robot.motion.set_yaw_control(True)

    def reset(self):
        pass

    def is_finished(self):
        return self.pid_surge.inrange(0.15) \
            and self.pid_sway.inrange(0.15) \
            and self.pid_yaw.inrange(0.20) \
            and abs(self.target_depth - self.robot.motion.get_depth()) < 0.25
