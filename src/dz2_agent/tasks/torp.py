import time
import task

class TestTorpedo(task.Task):
    def __init__(self, robot, torpedo, dt=0.1):
        super(self.__class__, self).__init__(robot, dt)

        self.torpedo = torpedo
        self.timeout = 0.5

    def setup(self):
        print "Firing torpedo {0}".format(self.torpedo)
        self.start = time.time()
        self.robot.pneumatics.fire_torpedo(self.torpedo)

    def execute(self):
        pass

    def cleanup(self):
        print "Done!"

    def reset(self):
        pass

    def is_finished(self):
        return (time.time() > self.start + self.timeout)
