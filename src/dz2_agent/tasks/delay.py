import time
import task

class Delay(task.Task):
    def __init__(self, robot, delay, name='Delay', dt=0.1):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

        self.delay = delay

    def setup(self):
        print "Delaying for {0} seconds".format(self.delay)
        self.start = time.time()

    def execute(self):
        pass

    def cleanup(self):
        pass

    def reset(self):
        pass

    def is_finished(self):
        return (time.time() - self.start) > self.delay
