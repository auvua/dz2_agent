#!/usr/bin/env python

import threading
import time
import abc

class Task(threading.Thread):
    __metaclass__ = abc.ABCMeta

    def __init__(self, robot, name, dt=0.02):
        threading.Thread.__init__(self)

        self.daemon = True
        self._interrupted = False
        self._running = False
        self.robot = robot
        self.name = name
        self.dt = 0.02
        self.next_task = None

        return self

    def run(self):
        self._running = True
        self.setup()
        while not self.is_finished() and not self._interrupted:
            self.execute()
            time.sleep(self.dt)
        self.cleanup()
        self._interrupted = False
        self._running = False

    @abc.abstractmethod
    def setup(self):
        return

    @abc.abstractmethod
    def execute(self):
        return

    @abc.abstractmethod
    def cleanup(self):
        return

    @abc.abstractmethod
    def is_finished(self):
        return

    @abc.abstractmethod
    def reset(self):
        return

    def interrupt(self):
        self._interrupted = True

    def is_running(self):
        return self._running

    def follow(self, task):
        if type(task) is Task:
            task.next_task = self
