import time
import Queue

class PID:
    def __init__(self, kp=0.0, ki=0.0, kd=0.0, lower=-1.0, upper=1.0):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.lower = lower
        self.upper = upper
        self.err = 0.0
        self.err_prev = 0.0
        self.err_int = 0.0
        self.err_int_max = self.upper
        self.err_der = 0.0
        self.time_prev = time.time()
        self.value_prev = 0.0
        self.output = 0.0
        self.target = 0.0
        self.enable = False
        self._inrange = InRange(100)

    def _bound(self, value):
        if value > self.upper:
            return self.upper
        elif value < self.lower:
            return self.lower
        else:
            return value

    def set_target(self, value):
        self.target = value

    def inrange(self, range):
        return self._inrange.get_avg_abs_err() < range and self._inrange.valid()

    def update(self, time_curr, value):
        if not self.enable:
            return 0

        self._inrange.update(value)
        time_delta = time_curr - self.time_prev
        self.err = self.target - value
        if time_delta > 0:
            self.err_der = (self.err - self.err_prev) / time_delta
        self.err_int = self._bound(self.err_int + (self.err * time_delta))

        self.output = self.kp*self.err + self.ki*self.err_int + self.kd*self.err_der

        self.value_prev = value
        self.err_prev = self.err
        self.time_prev = time_curr

        return self._bound(self.output)

    def reset(self):
        self.output = 0.0
        self.err = 0.0
        self.err_int = 0.0
        self.err_prev = 0.0
        self.value_prev = 0.0
        self._inrange.reset()

class InRange():
    def __init__(self, size):
        self.size = size
        self.reset()

    def update(self, value):
        if self.queue.full():
            tail = self.queue.get()
            self.abssum = self.abssum - abs(tail)
        self.queue.put(value)
        self.abssum = self.abssum + abs(value)

    def get_avg_abs_err(self):
        return self.abssum / self.queue.qsize() if self.queue.qsize() > 0 else 0

    def valid(self):
        return self.queue.full()

    def reset(self):
        self.queue = Queue.Queue(self.size)
        self.abssum = 0.0
