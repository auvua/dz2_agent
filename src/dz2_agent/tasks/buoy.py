import time
import task
from utils.pid import PID
from geometry_msgs.msg import Wrench

class MoveToBuoy(task.Task):
    def __init__(self, robot, camera, filter_name, surge, sway, heave, ramp, dt=0.1):
        super(self.__class__, self).__init__(robot, dt)
        self.filter_name = filter_name
        self.camera = camera
        self.force = Wrench()
        self.force.force.x = surge
        self.force.force.y = sway
        self.force.force.z = heave
        self.ramp = ramp
        self.filtered = {}

    def setup(self):
        self.robot.motion.set_all_control(True)
        self.start_time = time.time()

    def execute(self):
        self.filtered = self.robot.vision.process(self.camera, self.filter_name)

        ramp_force = Wrench()

        if self.ramp > 0:
            scaling = min((time.time() - self.start_time) / self.ramp, 1.0)
        else:
            scaling = 1.0

        ramp_force.force.x = self.force.force.x * scaling
        ramp_force.force.y = self.force.force.y * scaling
        ramp_force.force.z = self.force.force.z * scaling

        self.robot.motion.move_surge(ramp_force.force.x)
        self.robot.motion.move_sway(ramp_force.force.y)
        self.robot.motion.move_heave(ramp_force.force.z)

    def cleanup(self):
        self.robot.motion.clear_force()

    def reset(self):
        pass

    def is_finished(self):
        return 'buoy' in self.filtered and self.filtered['buoy']


class AlignToBuoy(task.Task):
    def __init__(self, robot, camera, filter_name, radius=100, dt=0.1):
        super(self.__class__, self).__init__(robot, dt)
        self.filter_name = filter_name
        self.camera = camera
        self.radius = radius
        self.last_msg = time.time()
        self.last_seen = time.time()
        self.cx = -1.0
        self.cy = -1.0
        self.radius = -1.0

        #TODO: tune these!
        self.pid_surge = PID(0.003, 0.0, 0.0, -50.0, 50.0)
        self.pid_sway = PID(2.0, 0.01, 0.0, -50.0, 50.0)
        self.pid_heave = PID(0.5, 0.0015, 0.0, -50.0, 50.0)

    def setup(self):
        self.start = time.time()
        self.robot.motion.set_all_control(True)
        self.robot.motion.set_depth_control(False)
        self.pid_surge.enable = True
        self.pid_sway.enable = True
        self.pid_heave.enable = True
        self.pid_surge.set_target(60.0)

    def execute(self):
        curr_time = time.time()
        filtered = self.robot.vision.process(self.camera, self.filter_name)
        self.robot.vision.publish('buoyfilter', filtered['image'])

        if 'buoy' in filtered and filtered['buoy']:
            keys = ['cx', 'cy', 'radius', 'fit']
            cx, cy, radius, fit = [filtered[k] for k in keys]
            self.cx = cx
            self.cy = cy
            self.radius = radius
            print("Buoy found")

            if self.pid_surge.enable is False:
                self.robot.leds.set_leds_strobe(255, 0, 0, time=0.2)

            self.last_seen = time.time()
            self.pid_surge.enable = True
            self.pid_sway.enable = True
            self.pid_heave.enable = True
            self.robot.motion.move_surge(self.pid_surge.update(curr_time, radius))
            self.robot.motion.move_sway(self.pid_sway.update(curr_time, cx))
            self.robot.motion.move_yaw(self.pid_heave.update(curr_time, cy))
        else:
            if curr_time - self.last_msg > 1.0:
                self.last_msg = curr_time
                print("Searching for buoy")
                self.robot.leds.set_leds_strobe(255, 0, 0, time=0.2)
            if curr_time - self.last_msg > 0.5:
                self.pid_surge.enable = False
                self.pid_sway.enable = False
                self.pid_heave.enable = False
                self.robot.motion.move_surge(0.0)
                self.robot.motion.move_sway(0.0)
                self.robot.motion.move_yaw(0.0)

    def cleanup(self):
        self.robot.motion.clear_force()
        self.robot.motion.set_depth_control(True)

    def reset(self):
        pass

    def is_finished(self):
        return abs(self.cx) < 0.3 and abs(self.cy) < 0.3 and self.radius > 40.0 and self.radius < 80.0
        #return self.pid_surge.inrange(20) and self.pid_sway.inrange(0.3) and self.pid_heave.inrange(0.3)
