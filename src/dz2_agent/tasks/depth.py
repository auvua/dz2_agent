import task
import rospy

class GoToDepth(task.Task):
    def __init__(self, robot, depth, name='GoToDepth', dt=0.1):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

        self.target_depth = depth

    def setup(self):
        rospy.loginfo("Going to depth: {0}".format(self.target_depth))
        self.robot.motion.set_depth(self.target_depth)
        self.robot.motion.set_depth_control(True)

    def execute(self):
        rospy.loginfo("Robot depth is: {0}".format(self.robot.motion.get_depth()))

    def cleanup(self):
        pass

    def reset(self):
        pass

    def is_finished(self):
        depth = self.robot.motion.get_depth()
        return abs(self.target_depth - depth) < 0.05


class SetDepth(task.Task):
    def __init__(self, robot, depth, name='SetDepth', dt=0.1):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

        self.target_depth = depth

    def setup(self):
        rospy.loginfo("Setting depth: {0}".format(self.target_depth))
        self.robot.motion.set_depth(self.target_depth)
        self.robot.motion.set_depth_control(True)

    def execute(self):
        pass

    def cleanup(self):
        pass

    def reset(self):
        pass

    def is_finished(self):
        return True
