import time
import task
import rospy

class TestLEDs(task.Task):
    def __init__(self, robot, dt=0.1, name='leds'):
        super(self.__class__, self).__init__(robot, name, dt)

    def setup(self):
        rospy.loginfo("Sending light command")
        r.leds.set_leds(100, 100, 100, time=1, cycles=1)

    def execute(self):
        pass

    def cleanup(self):
        rospy.loginfo("Done!")

    def reset(self):
        pass

    def is_finished(self):
        return True
