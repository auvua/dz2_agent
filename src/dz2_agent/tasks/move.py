import task
import time
import math
import rospy
import tf
from geometry_msgs.msg import Wrench, Pose

class LockPose(task.Task):
    def __init__(self, robot, name='LockPose', dt=0.1):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

    def setup(self):
        rospy.loginfo("Setting target pose to current pose")
        pose = self.robot.motion.get_pose()
        self.robot.motion.set_all_control(True)
        self.robot.motion.set_pose_absolute(pose)

    def execute(self):
        pass

    def cleanup(self):
        pass

    def reset(self):
        pass

    def is_finished(self):
        return True


class LockHeading(task.Task):
    def __init__(self, robot, name='LockHeading', dt=0.1):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

    def setup(self):
        rospy.loginfo("Setting target heading to current heading, zero pitch/roll")
        pose = self.robot.motion.get_pose()
        self.robot.motion.set_all_control(True)
        x = pose.orientation.x
        y = pose.orientation.y
        z = pose.orientation.z
        w = pose.orientation.w
        heading = tf.transformations.euler_from_quaternion((x, y, z, w))[2]
        q = tf.transformations.quaternion_from_euler(0, 0, heading)
        pose.orientation.x = q[0]
        pose.orientation.y = q[1]
        pose.orientation.z = q[2]
        pose.orientation.w = q[3]
        self.robot.motion.set_pose_absolute(pose)

    def execute(self):
        pass

    def cleanup(self):
        pass

    def reset(self):
        pass

    def is_finished(self):
        return True


class Move(task.Task):
    def __init__(self, robot, surge, sway, heave, time, ramp, name='MoveFwd', dt=0.1):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

        self.force = Wrench()
        self.force.force.x = surge
        self.force.force.y = sway
        self.force.force.z = heave
        self.time = time
        self.ramp = ramp

    def setup(self):
        rospy.loginfo("Moving for {0} seconds with force {1}".format(self.time, self.force))
        self.start_time = time.time()

    def execute(self):
        ramp_force = Wrench()

        if self.ramp > 0:
            scaling = min((time.time() - self.start_time) / self.ramp, 1.0)
        else:
            scaling = 1.0

        ramp_force.force.x = self.force.force.x * scaling
        ramp_force.force.y = self.force.force.y * scaling
        ramp_force.force.z = self.force.force.z * scaling

        self.robot.motion.move_surge(ramp_force.force.x)
        self.robot.motion.move_sway(ramp_force.force.y)
        self.robot.motion.move_heave(ramp_force.force.z)

    def cleanup(self):
        rospy.loginfo("Stopping movement")
        self.robot.motion.clear_force()

    def reset(self):
        pass

    def is_finished(self):
        return (time.time() - self.start_time) > self.time


class BarrelRoll(task.Task):
    def __init__(self, robot, speed=0.5, time=5.0, cw=True, name='BarrelRoll', dt=0.05):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

        self.pose = Pose()
        self.initial_pose = Pose()
        self.initial_pose_name = 'task_barrel_roll'

        self.speed = speed
        self.time = time
        self.cw = cw

    def setup(self):
        rospy.loginfo("Performing barrel roll over {0} seconds at speed {1}".format(self.time, self.speed))
        self.start_time = time.time()
        self.initial_pose = self.robot.motion.get_pose()
        self.robot.motion.set_static_frame(self.initial_pose, self.initial_pose_name)
        self.robot.motion.set_all_control(True)
        self.robot.motion.move_surge(self.speed)

    def execute(self):
        target_pose = Pose()
        elapsed = time.time() - self.start_time
        dir = 1.0 if self.cw else -1.0
        angle = dir*2.0*math.pi*elapsed/self.time
        q = tf.transformations.quaternion_from_euler(angle, 0.0, 0.0)
        target_pose.orientation.x = q[0]
        target_pose.orientation.y = q[1]
        target_pose.orientation.z = q[2]
        target_pose.orientation.w = q[3]
        print target_pose
        self.robot.motion.set_pose_from_frame(target_pose, self.initial_pose_name)

    def cleanup(self):
        rospy.loginfo("Stopping movement")
        self.robot.motion.clear_force()
        self.robot.motion.set_pose_absolute(self.initial_pose)

    def reset(self):
        pass

    def is_finished(self):
        return (time.time() - self.start_time) > self.time


class RotateRelative(task.Task):
    def __init__(self, robot, angle=0.0, time=1.0, name='RotateRelative', dt=0.05):
        super(self.__class__, self).__init__(robot, name=name, dt=dt)

        self.time = time
        self.angle = math.radians(angle)
        self.pose = Pose()
        self.initial_pose = Pose()

    def setup(self):
        rospy.loginfo("Rotating {0} degrees".format(math.degrees(self.angle)))
        self.start_time = time.time()
        self.initial_pose = self.robot.motion.get_pose()
        rospy.loginfo("Initial pose: {0}".format(self.initial_pose))
        self.robot.motion.set_static_frame(self.initial_pose, 'rotate_relative')
        self.pose = self.robot.motion.set_pose_orientation(self.pose, 0.0, 0.0, self.angle)
        self.robot.motion.set_pose_from_frame(self.pose, 'rotate_relative')
        self.robot.motion.set_all_control(True)

    def execute(self):
        pass

    def cleanup(self):
        pass

    def reset(self):
        pass

    def is_finished(self):
        return (time.time() - self.start_time) > self.time
