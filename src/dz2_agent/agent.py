import rospy
import robot
import sys
import time
import importlib

def mission_path(name):
    return "~mission/missions/%s" % name


def load_mission(mission_name, robot):
    tasks = []
    keys = ['module', 'class', 'params']
    mission = rospy.get_param(mission_path(mission_name))
    for id, task in enumerate(mission, 1):
        (module_name, class_name, params) = [task.get(key) for key in keys]
        try:
            module = importlib.import_module(module_name)
            klass = getattr(module, class_name)
            if params:
                task_instance = klass(robot, **params)
            else:
                task_instance = klass(robot)
            task_instance.id = id
            tasks.append(task_instance)
        except ImportError:
            rospy.logfatal("Unable to load task module %s", module_name)
        except AttributeError:
            rospy.logfatal("Unable to load task class %s", class_name)
    return tasks

class LaunchTask(object):
    def run(self, robot):
        if len(robot.mission):
            robot.current_task = robot.mission.pop(0)
            robot.current_task.start()
            rospy.loginfo("Starting task [%d] %s", robot.current_task.id, robot.current_task.__class__.__name__)

        else:
            robot.current_task = None
            rospy.loginfo("Mission finished")
            robot.leds.set_leds_const(0, 255, 0, time=2.0)

    def next(self, robot):
        if robot.current_task:
            return TASK_WAITING
        else:
            return STOPPED

class WaitForTask(object):
    def __init__(self):
        robot.current_task = None

    def run(self, robot):
        pass

    def next(self, robot):
        if not robot.sensors.get_mission():
            rospy.loginfo("Cancelling mission")
            robot.motion.set_all_control(False)
            return WAITING

        if not robot.current_task.is_running():
            rospy.loginfo("Finished task [%d] %s", robot.current_task.id, robot.current_task.__class__.__name__)
            robot.leds.set_leds_fullfade(0, 0, 255, time=1.0, cycles=2)
            return LAUNCHING
        return TASK_WAITING

class StartMission(object):
    def run(self, robot):
        mission_name = rospy.get_param("~mission/enabled")
        rospy.loginfo("Starting Mission: %s", mission_name)
        robot.mission = load_mission(mission_name, robot)
        robot.leds.set_leds_fullfade(0, 255, 0, time=1.0, cycles=3)

    def next(self, robot):
        if robot.sensors.get_mission():
            rospy.loginfo("Launching tasks")
            return LAUNCHING
        else:
            rospy.loginfo("Cancelling mission")
            robot.motion.set_all_control(False)
            return WAITING

class Waiting(object):
    def __init__(self):
        self.last_time = time.time()

    def run(self, robot):
        robot.mission = None
        if time.time() - self.last_time > 1.0:
            robot.leds.set_leds_fullfade(255, 255, 255, time=0.5, cycles=1.0)
            self.last_time = time.time()
        if robot.current_task:
            robot.current_task.interrupt()
            robot.current_task = None

    def next(self, robot):
        if robot.sensors.get_mission():
            return STARTING
        else:
            return WAITING

class Stopped(object):
    def run(self, robot):
        pass

    def next(self, robot):
        if robot.sensors.get_mission():
            return STOPPED
        else:
            rospy.loginfo("Waiting for mission switch")
            robot.motion.set_all_control(False)
            return WAITING

TASK_WAITING = WaitForTask()
STARTING = StartMission()
LAUNCHING = LaunchTask()
WAITING = Waiting()
STOPPED = Stopped()

class Runloop(object):
    def __init__(self, initialState):
        self.rate = rospy.Rate(20)
        self.state = initialState

    def run(self, robot):
        self.state.run(robot)
        while not rospy.is_shutdown():
            self.state = self.state.next(robot)
            self.state.run(robot)
            self.rate.sleep()

def main():
    rospy.init_node('dz2_agent', log_level=rospy.INFO)
    rate = rospy.Rate(20)

    rospy.loginfo("Loading robot model")
    r = robot.Robot()
    r.current_task = None
    r.load()

    mission = None
    statemachine = Runloop(STOPPED)
    statemachine.run(r)
    r.shutdown()

if __name__=='__main__':
    main()
