from base_filter import Filter

import numpy as np
import cv2
import math

class BuoyFilter(Filter):
    def __init__(self, name, hue_lower=170, hue_upper=20, saturation_lower=10,
                 saturation_upper=255, value_lower=0, value_upper=255):
        super(BuoyFilter, self).__init__(name)
        self.name=name
        self.hue_lower = hue_lower
        self.hue_upper = hue_upper
        self.saturation_lower = saturation_lower
        self.saturation_upper = saturation_upper
        self.value_lower = value_lower
        self.value_upper = value_upper

    def _confirm_contour(self, contour):
        area = cv2.contourArea(contour)
        circle = cv2.minEnclosingCircle(contour)

        if area > 100 and circle:
            print circle
            (cx, cy) = circle[0]
            radius = circle[1]
            circle_area = math.pi*(radius**2)
            fit = area / circle_area

            if fit > 0.6:
                return [cx, cy, radius, fit]

        return (None, None, None, None)

    def apply(self, image):
        img = cv2.cvtColor(image,  cv2.COLOR_BGR2HSV)
        img = cv2.blur(img, (3, 3))
        lower = np.array([self.hue_lower, self.saturation_lower, self.value_lower])
        upper = np.array([self.hue_upper, self.saturation_upper, self.value_upper])
        pic = self._hsv_in_range(img, lower, upper)
        pic = cv2.dilate(pic, np.ones((3, 3), np.uint8), iterations=3)
        pic = cv2.erode(pic, np.ones((3, 3), np.uint8), iterations=3)
        output = pic.copy()

        pic_height, pic_width = pic.shape[:2]

        contours = cv2.findContours(pic, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0]

        for contour in contours:
            cx, cy, radius, fit = self._confirm_contour(contour)
            if cx is None or cy is None:
                continue
            cv2.drawContours(image, [contour], 0, (0,255,0), 3)
            cx = 2.0*cx/pic_width - 1
            cy = 2.0*cy/pic_height - 1
            return dict(buoy=True, image=image, cx=cx, cy=cy, radius=radius, fit=fit)
        return dict(buoy=False, image=output)
