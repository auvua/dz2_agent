from base_filter import Filter

import numpy as np
import cv2
import math

class BinFilter(Filter):
    def __init__(self, name, hue_lower=0, hue_upper=255, saturation_lower=0,
                 saturation_upper=80, value_lower=120, value_upper=255):
        super(BinFilter, self).__init__(name)
        self.name=name
        self.hue_lower = hue_lower
        self.hue_upper = hue_upper
        self.saturation_lower = saturation_lower
        self.saturation_upper = saturation_upper
        self.value_lower = value_lower
        self.value_upper = value_upper

    def _get_theta(self, rect):
        rect_theta = rect[2]
        rect_width, rect_height  = rect[1]
        theta = 0.0

        if rect_height > rect_width:
            theta = rect_theta / 180.0 * math.pi
        else:
            theta = (rect_theta + 90.0) / 180.0 * math.pi
        return theta

    def _find_bin(self, contours, hierarchy):
        if hierarchy is None:
            return (None, None, None, None, None)
        hierarchy = hierarchy[0]
        chdata = zip(contours, hierarchy)

        cx = None
        cy = None
        theta = None
        area = None
        contour = None

        for component in chdata:
            c = component[0]
            h = component[1]
            #if h[2] < 0:
            if h[3] < 0:
                continue
            p = chdata[h[3]][0]
            crect = cv2.minAreaRect(c)
            prect = cv2.minAreaRect(p)

            dist = math.sqrt((crect[0][0] - prect[0][0])**2 + (crect[0][1] - prect[0][1])**2)
            area_ratio = (crect[1][0]*crect[1][1])/(prect[1][0]*prect[1][1])
            if area_ratio > 0.20 and area_ratio < 0.5 and dist < 20:
                print("Dist: {0}\tRatio: {1}".format(dist, area_ratio))
                cx = crect[0][0]
                cy = crect[0][1]
                contour = c
                theta = self._get_theta(crect)
                area = crect[1][0] * crect[1][1]
                break
        return (cx, cy, theta, area, contour)

    def apply(self, image):
        img = cv2.cvtColor(image,  cv2.COLOR_BGR2HSV)
        img = cv2.blur(img, (3, 3))
        lower = np.array([self.hue_lower, self.saturation_lower, self.value_lower])
        upper = np.array([self.hue_upper, self.saturation_upper, self.value_upper])
        pic = self._hsv_in_range(img, lower, upper)
        test = pic.copy()
        pic = cv2.erode(pic, np.ones((3,3), np.uint8), iterations=2)
        pic = cv2.dilate(pic, np.ones((3,3), np.uint8), iterations=2)
        output = image.copy()

        pic_height, pic_width = pic.shape[:2]

        contours, hierarchy = cv2.findContours(pic, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        cx, cy, theta, area, contour = self._find_bin(contours, hierarchy)

        if cx is None or cy is None:
            return dict(bin=False, image=test)

        cv2.drawContours(output, [contour], 0, (0,255,0), 3)
        cx = 2.0*cx/pic_width - 1
        cy = 2.0*cy/pic_height - 1
        return dict(bin=True, image=output, cx=cx, cy=cy, theta=theta, area=area)
