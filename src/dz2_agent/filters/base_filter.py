from abc import ABCMeta, abstractproperty
import numpy as np
import cv2

class Filter(object):
    __metaclass__ = ABCMeta

    def __init__(self, name):
        self.name = name

    def _hsv_in_range(self, image, lower, upper):
        range_low1 =  np.array([0, lower[1], lower[2]])
        range_high1 = np.array([upper[0], upper[1], upper[2]])
        range_low2 =  np.array([lower[0], lower[1], lower[2]])
        range_high2 = np.array([255, upper[1], upper[2]])
        if lower[0] <= upper[0]:
            return cv2.inRange(image, range_low2, range_high1)
        else:
            res1 = cv2.inRange(image, range_low1, range_high1)
            res2 = cv2.inRange(image, range_low2, range_high2)
            return cv2.bitwise_or(res1, res2)

    @abstractproperty
    def apply(self, data):
        """
        Filter function to be applied"
        """
