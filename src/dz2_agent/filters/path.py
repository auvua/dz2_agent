from base_filter import Filter

import numpy as np
import cv2
import math

class PathFilter(Filter):
    def __init__(self, name, hue_lower=0, hue_upper=60, saturation_lower=0,
                 saturation_upper=255, value_lower=0, value_upper=255, cm_method='area'):
        super(PathFilter, self).__init__(name)
        self.name=name
        self.hue_lower = hue_lower
        self.hue_upper = hue_upper
        self.saturation_lower = saturation_lower
        self.saturation_upper = saturation_upper
        self.value_lower = value_lower
        self.value_upper = value_upper
        self.cm_method = cm_method

    def _confirm_contour(self, contour):
        area = cv2.contourArea(contour)
        print area
        if area < 500:
            return (None, None, None, None)
        rect = cv2.minAreaRect(contour)
        rect_theta = rect[2]
        rect_width, rect_height  = rect[1]
        theta = 0.0
        width = 0.0
        length = 0.0
        if rect_height > rect_width:
            theta = rect_theta / 180.0 * math.pi
            length = rect_height
            width = rect_width
        else:
            theta = (rect_theta + 90.0) / 180.0 * math.pi
            length = rect_width
            width = rect_height
        xy = rect[0]
        cx = xy[0]
        cy = xy[1]
        if length <= 0 or width <= 0:
            return (None, None, None, None)
        aspectRatio = length/width
        rectangular = (area / (length * width))
        if aspectRatio >= 3  and aspectRatio <= 12 and rectangular > .5:
            print [cx, cy, theta, area]
            return cx, cy, theta, area
        else:
            return (None, None, None, None)

    def apply(self, image):
        img = cv2.cvtColor(image,  cv2.COLOR_BGR2HSV)
        img = cv2.blur(img, (3, 3))
        lower = np.array([self.hue_lower, self.saturation_lower, self.value_lower])
        upper = np.array([self.hue_upper, self.saturation_upper, self.value_upper])
        pic = self._hsv_in_range(img, lower, upper)
        output = pic.copy()

        pic_height, pic_width = pic.shape[:2]

        contours = cv2.findContours(pic, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0]

        for contour in contours:
            cx, cy, theta, area = self._confirm_contour(contour)
            if cx is None or cy is None:
                continue
            cv2.drawContours(image, [contour], 0, (0,255,0), 3)
            cx = 2.0*cx/pic_width - 1
            cy = 2.0*cy/pic_height - 1
            return dict(marker=True, image=image, cx=cx, cy=cy, theta=theta, area=area)
        return dict(marker=False, image=output)
