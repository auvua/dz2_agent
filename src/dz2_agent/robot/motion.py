import threading
import time
import copy
import Queue
import rospy
import tf
import tf2_ros
import tf2_geometry_msgs
import dynamic_reconfigure.client
from geometry_msgs.msg import Wrench, TransformStamped, Pose

class Motion(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

        self.daemon = True

        self.pid_en = {
            'pid_roll_en': False,
            'pid_pitch_en': False,
            'pid_yaw_en': False,
            'pid_depth_en': False
        }
        self.ref_frames = {}

        self.force = Wrench()
        self.target_pose = TransformStamped()
        self.target_pose.header.frame_id = 'world'
        q = tf.transformations.quaternion_from_euler(0, 0, 0)
        self.target_pose.transform.rotation.x = q[0]
        self.target_pose.transform.rotation.y = q[1]
        self.target_pose.transform.rotation.z = q[2]
        self.target_pose.transform.rotation.w = q[3]
        self.current_pose = Pose()

        self.ctrl_client = dynamic_reconfigure.client.Client('dz2_control', config_callback=self._client_callback)
        self.force_pub = rospy.Publisher('/cmd_force', Wrench, queue_size=10)
        self.tb = tf2_ros.TransformBroadcaster()
        self.listener = tf.TransformListener()

        self.start()

    def run(self):
        self.enabled = True

        while self.enabled:
            time.sleep(0.02)

            try:
                (trans, rot) = self.listener.lookupTransform('/world', '/base_link', rospy.Time(0))
                current_pose = Pose()
                current_pose.position.x = trans[0]
                current_pose.position.y = trans[1]
                current_pose.position.z = trans[2]
                current_pose.orientation.x = rot[0]
                current_pose.orientation.y = rot[1]
                current_pose.orientation.z = rot[2]
                current_pose.orientation.w = rot[3]
                self.current_pose = current_pose
            except (tf.Exception, tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                pass

            curr_rospy_time = rospy.Time.now()

            self.target_pose.header.stamp = curr_rospy_time
            self.target_pose.child_frame_id = 'goal'
            self.force_pub.publish(self.force)

            self.tb.sendTransform(self.target_pose)
            for frame_name, frame in self.ref_frames.iteritems():
                frame.header.stamp = curr_rospy_time
                self.tb.sendTransform(frame)

    def _client_callback(self, config):
        for en in self.pid_en.keys():
            self.pid_en[en] = config[en]
        return config

    def set_all_control(self, on):
        self.set_roll_control(on)
        self.set_pitch_control(on)
        self.set_yaw_control(on)
        self.set_depth_control(on)

    def set_roll_control(self, on):
        self.pid_en['pid_roll_en'] = on
        self.ctrl_client.update_configuration({'pid_roll_en': on})

    def set_pitch_control(self, on):
        self.pid_en['pid_pitch_en'] = on
        self.ctrl_client.update_configuration({'pid_pitch_en': on})

    def set_yaw_control(self, on):
        self.pid_en['pid_yaw_en'] = on
        self.ctrl_client.update_configuration({'pid_yaw_en': on})

    def set_depth_control(self, on):
        self.pid_en['pid_depth_en'] = on
        self.ctrl_client.update_configuration({'pid_depth_en': on})

    def set_pose_orientation(self, pose, roll, pitch, yaw):
        q = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
        pose.orientation.x = q[0]
        pose.orientation.y = q[1]
        pose.orientation.z = q[2]
        pose.orientation.w = q[3]
        return pose

    def get_depth(self):
        print(self.current_pose.position.z)
        return self.current_pose.position.z

    def get_pose(self):
        pose = Pose()
        pose.position = self.current_pose.position
        pose.orientation = self.current_pose.orientation
        return pose

    def remove_translation(self, transform):
        transform.transform.translation.x = 0.0
        transform.transform.translation.y = 0.0
        transform.transform.translation.z = 0.0

    def _set_transform_to_pose(self, transform, pose):
        transform.transform.translation.x = pose.position.x
        transform.transform.translation.y = pose.position.y
        transform.transform.translation.z = pose.position.z
        transform.transform.rotation.x = pose.orientation.x
        transform.transform.rotation.y = pose.orientation.y
        transform.transform.rotation.z = pose.orientation.z
        transform.transform.rotation.w = pose.orientation.w

    def set_pose_absolute(self, pose):
        abs_pose = TransformStamped()
        abs_pose.header.frame_id = 'world'
        abs_pose.child_frame_id = 'goal'
        self._set_transform_to_pose(abs_pose, pose)
        self.target_pose = abs_pose

    def set_pose_relative(self, pose):
        self.set_pose_from_frame(pose, 'base_link')

    def set_static_frame(self, pose, frame):
        new_pose = TransformStamped()
        new_pose.header.frame_id = 'world'
        new_pose.child_frame_id = frame
        self._set_transform_to_pose(new_pose, pose)
        self.ref_frames[frame] = new_pose

    def set_pose_from_frame(self, pose, source):
        rel_pose = TransformStamped()
        rel_pose.header.frame_id = source
        rel_pose.child_frame_id = 'goal'
        self._set_transform_to_pose(rel_pose, pose)
        self.target_pose = rel_pose
        print self.target_pose

    def set_depth(self, depth):
        depth_pose = copy.deepcopy(self.target_pose)
        depth_pose.header.frame_id = 'world'
        depth_pose.child_frame_id = 'goal'
        depth_pose.transform.translation.z = depth
        self.target_pose = depth_pose

    def set_force(self, surge, sway, heave, roll, pitch, yaw):
        force = Wrench()
        force.force.x = surge
        force.force.y = sway
        force.force.x = heave
        force.torque.x = roll
        force.torque.y = pitch
        force.torque.z = yaw
        self.force = force

    def clear_force(self):
        force = Wrench()
        self.force = force

    def move_surge(self, force):
        self.force.force.x = force

    def move_sway(self, force):
        self.force.force.y = force

    def move_heave(self, force):
        self.force.force.z = force

    def move_roll(self, force):
        self.force.torque.x = force

    def move_pitch(self, force):
        self.force.torque.y = force

    def move_yaw(self, force):
        self.force.torque.z = force

    def stop(self):
        self.enabled = False
