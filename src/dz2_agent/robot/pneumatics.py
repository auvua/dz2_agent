import threading
import time
import Queue
import rospy
from rospy_tutorials.msg import Floats

class Pneumatics(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

        self.daemon = True
        self.pneumatics = [0.0 for i in range(10)]
        self.queue = Queue.Queue()

        self.pneumatics_pub = rospy.Publisher('/pneumatics', Floats, queue_size=5)

        self.start()

    def run(self):
        self.enabled = True
        while self.enabled:
            time.sleep(0.02)
            while not self.queue.empty():
                port = self.queue.get()
                self.pneumatics[port] = 1.0
                self.pneumatics_pub.publish(self.pneumatics)
                if port == 0 or port == 1:
                    time.sleep(0.05)
                else:
                    time.sleep(0.02)
                self.pneumatics[port] = 0.0
                self.pneumatics_pub.publish(self.pneumatics)
                time.sleep(0.02)
            self.pneumatics_pub.publish(self.pneumatics)

    def fire_torpedo(self, torpedo):
        if torpedo == 1 or torpedo == 2:
            self.queue.put(torpedo - 1)

    def fire_marker(self, marker):
        if marker == 1 or marker == 2:
            self.queue.put(marker + 1)

    def open_claw(self):
        self.pneumatics[5] = 0.0

    def close_claw(self):
        self.pneumatics[5] = 1.0

    def stop(self):
        self.enabled = False
