import time
import rospy
from dz2_leds.msg import LEDCmd

class LEDs():
    def __init__(self):
        self.led_pub = rospy.Publisher('/ledcmd', LEDCmd, queue_size=10)

    def set_leds(self, r, g, b, style=LEDCmd.PATTERN_CONST, time=1.0, cycles=1.0):
        cmd = LEDCmd()
        cmd.pattern = style
        cmd.color.r = r
        cmd.color.g = g
        cmd.color.b = b
        cmd.time = time
        cmd.cycles = cycles
        self.led_pub.publish(cmd)

    def set_leds_const(self, r, g, b, time=1.0):
        self.set_leds(r, g, b, style=LEDCmd.PATTERN_CONST, time=time)

    def set_leds_strobe(self, r, g, b, time=1.0, cycles=1.0):
        self.set_leds(r, g, b, style=LEDCmd.PATTERN_STROBE, time=time, cycles=cycles)

    def set_leds_fullfade(self, r, g, b, time=1.0, cycles=1.0):
        self.set_leds(r, g, b, style=LEDCmd.PATTERN_FULLFADE, time=time, cycles=cycles)
