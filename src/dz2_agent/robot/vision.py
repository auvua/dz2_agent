import importlib
import threading

import rospy

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

def convert_to_cv_image(data):
    result = None
    try:
        result = bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
        print(e)
    return result

def convert_to_ros_image(data):
    result = None
    try:
        result = bridge.cv2_to_imgmsg(data)
    except CvBridgeError as e:
        print(e)
    return result

class FilterNotFound(Exception):
    pass


class CameraNotFound(Exception):
    pass


def load_filters(filter_path):
    filters = {}
    keys = ['module', 'class', 'name', 'params']
    filter_entries = rospy.get_param(filter_path)
    for filter in filter_entries:
        (module_name, class_name, name, params) = [filter.get(key) for key in keys]
        try:
            module = importlib.import_module(module_name)
            klass = getattr(module, class_name)
            if params:
                filters[name] = klass(name, **params)
            else:
                filters[name] = klass(name)
        except ImportError:
            rospy.logfatal("Unable to load filter module %s", module_name)
        except AttributeError:
            rospy.logfatal("Unable to load filter class %s", class_name)
    return filters

def attach(orientation, fn):
    def wrapper(data):
        return fn(orientation, data)
    return wrapper

class VisionProcessor(threading.Thread):
    def __init__(self, filter_path, camera_path):
        threading.Thread.__init__(self)
        self.filters = load_filters(filter_path)
        self.camera_path = camera_path
        self.cameras = {}
        self.start()
        self.publishers = {}

    def update(self, orientation, data):
        self.cameras[orientation] = convert_to_cv_image(data)

    def run(self):
        self.cameras = rospy.get_param(self.camera_path)
        for (orientation, path) in self.cameras.items():
            print("Subscribing to {0} for camera {1}".format(path, orientation))
            rospy.Subscriber(path, Image, attach(orientation, self.update))
        rospy.spin()

    def process(self, camera, filter):
        if not self.filters.get(filter):
            raise FilterNotFound("The filter %s could not be found" % filter)
        if camera not in self.cameras:
            print self.cameras
            print self.filters
            raise CameraNotFound("The camera %s could not be found" % camera)
        filter = self.filters[filter]
        camera = self.cameras[camera]
        return filter.apply(camera)
    
    def publish(self, topic, image):
        if not topic in self.publishers:
            self.publishers[topic] = rospy.Publisher("~" + topic, Image, queue_size=10)
        self.publishers[topic].publish(convert_to_ros_image(image))
