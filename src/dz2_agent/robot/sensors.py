import rospy
from std_msgs.msg import Bool, Float64

class Sensors():
    def __init__(self):
        self.mission = False
        self.depth = 0.0

        self.mission_sub = rospy.Subscriber('/mission', Bool, self._callback_mission)
        self.depth_sub = rospy.Subscriber('/depth', Float64, self._callback_depth)

    def _callback_depth(self, msg):
        self.depth = msg.data

    def _callback_mission(self, msg):
        self.mission = msg.data

    def get_depth(self):
        return self.depth

    def get_mission(self):
        return self.mission

    def get_imu(self):
        pass

    def get_gyro(self):
        pass

    def get_accel(self):
        pass
