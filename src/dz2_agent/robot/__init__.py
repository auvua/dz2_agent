from sensors import Sensors
from pneumatics import Pneumatics
from motion import Motion
from leds import LEDs
import vision

class Robot():
    def load(self):
        self.sensors = sensors.Sensors()
        self.pneumatics = pneumatics.Pneumatics()
        self.motion = motion.Motion()
        self.leds = leds.LEDs()

        self.vision = vision.VisionProcessor("~filters", "~cameras")

    def shutdown(self):
        self.pneumatics.stop()
        self.motion.stop()
